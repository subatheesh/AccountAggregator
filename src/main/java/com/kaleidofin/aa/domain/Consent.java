package com.kaleidofin.aa.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "consent")
public class Consent extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "aa_consent_id")
    private String aaConsentId;

    @Column(name = "consent_handle")
    private String consentHandle;

    @Column(name = "is_data_fetched")
    private Boolean isDataFetched;

    @Column(name = "status")
    private String status;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "account_id")
    private String accountId;

    @Column(name = "type")
    private String consentType;

    @Column(name = "frequency")
    private String frequency;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getAaConsentId() {
        return aaConsentId;
    }

    public void setAaConsentId(String aaConsentId) {
        this.aaConsentId = aaConsentId;
    }

    public String getConsentHandle() {
        return consentHandle;
    }

    public void setConsentHandle(String consentHandle) {
        this.consentHandle = consentHandle;
    }

    public Boolean getDataFetched() {
        return isDataFetched;
    }

    public void setDataFetched(Boolean dataFetched) {
        isDataFetched = dataFetched;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getConsentType() {
        return consentType;
    }

    public void setConsentType(String consentType) {
        this.consentType = consentType;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "Consent{" +
                "id=" + id +
                ", version=" + version +
                ", customerId=" + customerId +
                ", aaConsentId='" + aaConsentId + '\'' +
                ", consentHandle='" + consentHandle + '\'' +
                ", isDataFetched=" + isDataFetched +
                ", status='" + status + '\'' +
                ", productId='" + productId + '\'' +
                ", accountId='" + accountId + '\'' +
                ", consentType='" + consentType + '\'' +
                ", frequency='" + frequency + '\'' +
                '}';
    }
}
