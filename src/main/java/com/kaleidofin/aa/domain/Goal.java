package com.kaleidofin.aa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "goals")
public class Goal extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "goal_name")
    private String goalName;

    @Column(name = "goal_amount")
    private BigDecimal goalAmount;

    @Column(name = "tenture")
    private Long tenture;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public BigDecimal getGoalAmount() {
        return goalAmount;
    }

    public void setGoalAmount(BigDecimal goalAmount) {
        this.goalAmount = goalAmount;
    }

    public Long getTenture() {
        return tenture;
    }

    public void setTenture(Long tenture) {
        this.tenture = tenture;
    }

    @Override
    public String toString() {
        return "Goal{" +
                "id=" + id +
                ", version=" + version +
                ", customerId=" + customerId +
                ", goalName='" + goalName + '\'' +
                ", goalAmount=" + goalAmount +
                ", tenture=" + tenture +
                '}';
    }
}
