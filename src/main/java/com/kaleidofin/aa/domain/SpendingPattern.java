package com.kaleidofin.aa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "spending_pattern")
public class SpendingPattern extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "category")
    private String category;

    @Column(name = "min_amount")
    private BigDecimal minAmount;

    @Column(name = "max_amount")
    private BigDecimal maxAmount;

    private BigDecimal amount;

    @Column(name = "spend_percentile")
    private Long spendPercentile;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }


    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Long getSpendPercentile() {
        return spendPercentile;
    }

    public void setSpendPercentile(Long spendPercentile) {
        this.spendPercentile = spendPercentile;
    }

    @Override
    public String toString() {
        return "SpendingPattern{" +
                "id=" + id +
                ", version=" + version +
                ", customerId=" + customerId +
                ", category='" + category + '\'' +
                ", minAmount=" + minAmount +
                ", maxAmount=" + maxAmount +
                ", amount=" + amount +
                ", spendPercentile=" + spendPercentile +
                '}';
    }
}
