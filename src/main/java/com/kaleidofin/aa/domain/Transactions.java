package com.kaleidofin.aa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
public class Transactions extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "mode")
    private String mode;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "type")
    private String type;

    @Column(name = "narration")
    private String narration;

    @Column(name = "reference_no")
    private String referenceNo;

    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    @Column(name = "transaction_fetch_timestamp")
    private LocalDateTime transactionFetchTimestamp;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public LocalDateTime getTransactionFetchTimestamp() {
        return transactionFetchTimestamp;
    }

    public void setTransactionFetchTimestamp(LocalDateTime transactionFetchTimestamp) {
        this.transactionFetchTimestamp = transactionFetchTimestamp;
    }

    @Override
    public String toString() {
        return "Transactions{" +
                "id=" + id +
                ", version=" + version +
                ", customerId=" + customerId +
                ", accountId=" + accountId +
                ", transactionId='" + transactionId + '\'' +
                ", mode='" + mode + '\'' +
                ", amount=" + amount +
                ", balance=" + balance +
                ", type='" + type + '\'' +
                ", narration='" + narration + '\'' +
                ", referenceNo='" + referenceNo + '\'' +
                ", transaction_date=" + transactionDate +
                ", transactionFetchTimestamp=" + transactionFetchTimestamp +
                '}';
    }
}
