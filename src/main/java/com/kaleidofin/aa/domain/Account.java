package com.kaleidofin.aa.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "account")
public class Account extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    private Integer version;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "consent_id")
    private Long consentId;

    @Column(name = "balance")
    private BigDecimal balance;

    @Column(name = "fip_name")
    private String fipName;

    @Column(name = "fip_id")
    private String fipId;

    @Column(name = "account_type")
    private String accountType;

    @Column(name = "link_ref_number")
    private String linkRefNumber;

    @Column(name = "masked_account_no")
    private String maskedAccountNo;

    @Column(name = "fi_type")
    private String fiType;

    @Column(name = "institute_name")
    private String instituteName;

    @Column(name = "account_holder_type")
    private String accountHolderType;

    @Column(name = "transaction_start_date")
    private LocalDate transactionStartDate;

    @Column(name = "transaction_end_date")
    private LocalDate transactionEndDate;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getConsentId() {
        return consentId;
    }

    public void setConsentId(Long consentId) {
        this.consentId = consentId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getFipName() {
        return fipName;
    }

    public void setFipName(String fipName) {
        this.fipName = fipName;
    }

    public String getFipId() {
        return fipId;
    }

    public void setFipId(String fipId) {
        this.fipId = fipId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getLinkRefNumber() {
        return linkRefNumber;
    }

    public void setLinkRefNumber(String linkRefNumber) {
        this.linkRefNumber = linkRefNumber;
    }

    public String getMaskedAccountNo() {
        return maskedAccountNo;
    }

    public void setMaskedAccountNo(String maskedAccountNo) {
        this.maskedAccountNo = maskedAccountNo;
    }

    public String getFiType() {
        return fiType;
    }

    public void setFiType(String fiType) {
        this.fiType = fiType;
    }

    public String getInstituteName() {
        return instituteName;
    }

    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }

    public String getAccountHolderType() {
        return accountHolderType;
    }

    public void setAccountHolderType(String accountHolderType) {
        this.accountHolderType = accountHolderType;
    }

    public LocalDate getTransactionStartDate() {
        return transactionStartDate;
    }

    public void setTransactionStartDate(LocalDate transactionStartDate) {
        this.transactionStartDate = transactionStartDate;
    }

    public LocalDate getTransactionEndDate() {
        return transactionEndDate;
    }

    public void setTransactionEndDate(LocalDate transactionEndDate) {
        this.transactionEndDate = transactionEndDate;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", version=" + version +
                ", customerId=" + customerId +
                ", consentId=" + consentId +
                ", balance=" + balance +
                ", fipName='" + fipName + '\'' +
                ", fipId='" + fipId + '\'' +
                ", accountType='" + accountType + '\'' +
                ", linkRefNumber='" + linkRefNumber + '\'' +
                ", maskedAccountNo='" + maskedAccountNo + '\'' +
                ", fiType='" + fiType + '\'' +
                ", instituteName='" + instituteName + '\'' +
                ", accountHolderType='" + accountHolderType + '\'' +
                ", transactionStartDate=" + transactionStartDate +
                ", transactionEndDate=" + transactionEndDate +
                '}';
    }
}
