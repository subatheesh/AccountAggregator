package com.kaleidofin.aa.web.rest;


import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.SpendingPattern;
import com.kaleidofin.aa.repository.SpendingPatternRepository;
import com.kaleidofin.aa.service.AuthenticationService;
import com.kaleidofin.aa.service.CustomerService;
import com.kaleidofin.aa.service.SpendingPatternService;
import com.kaleidofin.aa.service.dto.SpendingPatternGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerResource {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    CustomerService customerService;

    @Autowired
    SpendingPatternRepository spendingPatternRepository;

    @Autowired
    SpendingPatternService spendingPatternService;

    @GetMapping("/")
    public Customer getCustomer(@RequestHeader(value = "sessionId") String sessionId){
        return authenticationService.validateCustomer(sessionId);
    }

    @GetMapping("/update")
    public Customer updateCustomer(@RequestBody Customer customer, @RequestHeader(value = "sessionId") String sessionId){
        authenticationService.validateCustomer(sessionId);
        return customerService.updateCustomer(customer);
    }

    @GetMapping("/update/profile")
    public Customer updateCustomerProfile(@RequestParam(value = "profile") String profile, @RequestHeader(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return customerService.updateCustomerProfile(customer, profile);
    }

    @GetMapping("/defaultSpendingPattern")
    public SpendingPatternGroup getDefaultSpendingPattern(@RequestHeader(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        List<SpendingPattern> spendingPatternList = spendingPatternRepository.findByCustomerIdIsNull();
        spendingPatternList.forEach(sp -> sp.setId(null));
        return spendingPatternService.calculateAmount(spendingPatternList, customer);
    }

    @PostMapping("/saveSpendingPattern")
    public SpendingPatternGroup saveCustomerSpendingPattern(@RequestHeader(value = "sessionId") String sessionId,
                                                    @RequestBody List<SpendingPattern> spendingPatternList) {
        Customer customer = authenticationService.validateCustomer(sessionId);
        return spendingPatternService.saveSpendingPatterns(customer, spendingPatternList);
    }

    @GetMapping("/spendingPattern/overall")
    public SpendingPatternGroup getCustomerOverallSpendingPattern(@RequestHeader(value = "sessionId") String sessionId) {
        Customer customer = authenticationService.validateCustomer(sessionId);
        return spendingPatternService.getTransactionSpendingPattern(customer);
    }

    @GetMapping("/spendingPattern/{year}/{month}")
    public SpendingPatternGroup getCustomerSpendingPattern(@RequestHeader(value = "sessionId") String sessionId,
                                                         @PathVariable(value = "year") Integer year,
                                                         @PathVariable(value = "month") Integer month) {
        Customer customer = authenticationService.validateCustomer(sessionId);
        return spendingPatternService.getTransactionSpendingPattern(customer, year, month);
    }

    @GetMapping("/balance")
    public BigDecimal getCurrentBalance(@RequestParam(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return customerService.getCurrentBalance(customer);
    }

}
