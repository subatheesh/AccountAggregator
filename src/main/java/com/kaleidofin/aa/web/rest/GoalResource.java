package com.kaleidofin.aa.web.rest;


import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.Goal;
import com.kaleidofin.aa.service.AuthenticationService;
import com.kaleidofin.aa.service.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/api/customer/goal")
public class GoalResource {

    @Autowired
    GoalService goalService;

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping("/")
    public Goal createGoal(@RequestBody Goal goal, @RequestHeader(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return goalService.createGoal(goal);
    }

    @PostMapping("/create")
    public Goal createGoalForCustomer(@RequestParam(value = "goalName") String goalName,
                           @RequestParam(value = "goalAmount") BigDecimal goalAmount,
                           @RequestParam(value = "tenure") Long tenure,
                           @RequestHeader(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return goalService.createGoal(customer, goalName, goalAmount, tenure);
    }

    @GetMapping("/")
    public List<Goal> getGoals(@RequestHeader(value = "sessionId") String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return goalService.getGoals(customer);
    }
}
