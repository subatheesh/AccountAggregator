package com.kaleidofin.aa.web.rest;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationResource {

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping ("/")
    public Customer authorize(@RequestParam(value = "mobile") String mobile, @RequestParam(value = "sessionId") String sessionId) throws Exception {
       return authenticationService.authenticateUser(mobile, sessionId);
    }
}
