package com.kaleidofin.aa.web.rest;

import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.service.AuthenticationService;
import com.kaleidofin.aa.service.ConsentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/customer/consent")
public class ConsentResource {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    ConsentService consentService;

    @PostMapping("/mark")
    public Consent markConsent(@RequestParam(value = "productId", required = false) String productId,
                              @RequestParam(value = "accountId", required = true) String accountId,
                              @RequestHeader(value = "sessionId", required = true) String sessionId) throws URISyntaxException {
        Customer customer = authenticationService.validateCustomer(sessionId);
        Consent consent = consentService.createConsent(customer, accountId, productId, null);
        return consentService.checkConsentStatus(consent);
    }

    @GetMapping("/")
    public List<Consent> getConsent(@RequestParam(value = "productId", required = false) String productId,
                                    @RequestParam(value = "accountId", required = true) String accountId,
                                    @RequestHeader(value = "sessionId", required = true) String sessionId){
        Customer customer = authenticationService.validateCustomer(sessionId);
        return consentService.getConsent(customer, productId, accountId);
    }

    @PostMapping("/request")
    public Consent requestConsent(@RequestParam(value = "productId", required = false) String productId,
                                  @RequestParam(value = "accountId", required = true) String accountId,
                                  @RequestHeader(value = "sessionId", required = true) String sessionId) throws URISyntaxException {
        Customer customer = authenticationService.validateCustomer(sessionId);
        return consentService.requestConsent(customer, productId, accountId);
    }

}
