package com.kaleidofin.aa.utils;

import com.kaleidofin.aa.constants.AAConstants;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class RestTemplateUtils {

    public static HttpHeaders getAaHttpHeaders(){
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.set("client_id", AAConstants.AA_CLIENT_ID);
        httpHeaders.set("client_secret", AAConstants.AA_CLIENT_SECRET);
        httpHeaders.set("organisationId", AAConstants.AA_ORGANIZATION_ID);
        httpHeaders.set("appIdentifier", AAConstants.APP_IDENTIFIER);
        return httpHeaders;
    }

}
