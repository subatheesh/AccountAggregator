package com.kaleidofin.aa.constants;

public class AAConstants {

    public static final String AA_CLIENT_ID = "fp_test_fd46be92c0ff8bd86f8988778c9f932e7ca1204c";
    public static final String AA_CLIENT_SECRET = "fba95ad2563a7794391b7f29264a8733e680cc36d845089fd45b1c1306eb292d3b71b8d3";
    public static final String AA_ORGANIZATION_ID = "KAL0189";
    public static final String APP_IDENTIFIER = "com.kaleidofin.mobo";
    public static final String AA_PRODUCT_ID = "MOBOTWOID2";
    public static final String AA_PARTY_IDENTIFIER_TYPE = "MOBILE";
    public static final String AA_BASE_URL_V2 = "https://sandbox.moneyone.in/finpro_sandbox/v2/";
    public static final String AA_BASE_URL = "https://sandbox.moneyone.in/finpro_sandbox/";
}
