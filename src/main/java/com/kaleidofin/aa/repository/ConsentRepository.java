package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.Consent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ConsentRepository extends JpaRepository<Consent, Long> {

    List<Consent> findByStatus(String status);
    Optional<Consent> findByConsentHandle(String consentHandle);
    List<Consent> findByStatusAndIsDataFetched(String status, Boolean isDataFetched);
    List<Consent> findByCustomerIdAndAccountIdAndProductId(Long customerId, String accountId, String productId);

}
