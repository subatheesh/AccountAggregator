package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
