package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GoalsRepository  extends JpaRepository<Goal, Long> {

    List<Goal> findByCustomerId(Long customerId);

}
