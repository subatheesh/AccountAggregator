package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository  extends JpaRepository<Account, Long> {

    Account findByCustomerIdAndFipIdAndLinkRefNumber(Long customerId, String fipId, String linkRefNumber);
    List<Account> findByConsentId(Long consentId);
    List<Account> findByCustomerId(Long customerId);

}
