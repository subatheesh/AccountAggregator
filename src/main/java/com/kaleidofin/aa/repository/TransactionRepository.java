package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.Transactions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transactions, Long> {
    Transactions findByTransactionId(String transactionId);
    Transactions findByReferenceNo(String referenceNo);
    List<Transactions> findByCustomerId(Long customerId);

    @Query(value = "select * from transactions where customer_id=:customerId and YEAR(transaction_date)=:year and MONTH(transaction_date)=:month", nativeQuery = true)
    List<Transactions> findByCustomerIdForAMonth(@Param("customerId") Long customerId, @Param("year") Integer year, @Param("month") Integer month);
}
