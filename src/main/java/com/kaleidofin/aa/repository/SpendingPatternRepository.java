package com.kaleidofin.aa.repository;

import com.kaleidofin.aa.domain.SpendingPattern;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpendingPatternRepository extends JpaRepository<SpendingPattern, Long> {

    List<SpendingPattern> findByCustomerIdIsNull();
    List<SpendingPattern> findByCustomerId(Long customerId);

}
