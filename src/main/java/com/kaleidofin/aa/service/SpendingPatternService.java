package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.SpendingPattern;
import com.kaleidofin.aa.service.dto.SpendingPatternGroup;

import java.util.List;

public interface SpendingPatternService {

    SpendingPatternGroup getSpendingPattern(Long customerId);

    SpendingPatternGroup saveSpendingPatterns(Customer customer, List<SpendingPattern> spendingPatternList);

    SpendingPatternGroup calculateAmount(List<SpendingPattern> spendingPatternList, Customer customer);

    SpendingPattern calculateTotal(List<SpendingPattern> spendingPatternList);

    SpendingPatternGroup getTransactionSpendingPattern(Customer customer);

    SpendingPatternGroup getTransactionSpendingPattern(Customer customer, Integer year, Integer month);
}
