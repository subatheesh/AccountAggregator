package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Account;
import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.Transactions;

import java.net.URISyntaxException;
import java.util.List;

public interface TransactionService {

    void fetchTransactions(Consent consent);

    Consent fetchTransactions(Consent consent, Account account) throws URISyntaxException;

    List<Transactions> getTransactions(Customer customer);

    List<Transactions> getTransactions(Customer customer, Integer year, Integer month);
}
