package com.kaleidofin.aa.service.dto;

import com.kaleidofin.aa.domain.SpendingPattern;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SpendingPatternGroup {

    public SpendingPatternGroup(){
        this.spendingPatternList = new ArrayList<>();
    }

    private SpendingPattern totalSpendingPattern;
    private SpendingPattern savingSpendingPattern;
    private BigDecimal income;
    private List<SpendingPattern> spendingPatternList;

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public SpendingPattern getTotalSpendingPattern() {
        return totalSpendingPattern;
    }

    public void setTotalSpendingPattern(SpendingPattern totalSpendingPattern) {
        this.totalSpendingPattern = totalSpendingPattern;
    }

    public SpendingPattern getSavingSpendingPattern() {
        return savingSpendingPattern;
    }

    public void setSavingSpendingPattern(SpendingPattern savingSpendingPattern) {
        this.savingSpendingPattern = savingSpendingPattern;
    }

    public List<SpendingPattern> getSpendingPatternList() {
        return spendingPatternList;
    }

    public void setSpendingPatternList(List<SpendingPattern> spendingPatternList) {
        this.spendingPatternList = spendingPatternList;
    }
}
