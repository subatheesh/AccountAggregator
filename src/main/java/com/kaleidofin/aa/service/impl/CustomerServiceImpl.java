package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.constants.AAConstants;
import com.kaleidofin.aa.domain.Account;
import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.repository.AccountRepository;
import com.kaleidofin.aa.repository.ConsentRepository;
import com.kaleidofin.aa.repository.CustomerRepository;
import com.kaleidofin.aa.service.CustomerService;
import com.kaleidofin.aa.utils.RestTemplateUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    ConsentRepository consentRepository;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Customer getCustomer(Long customerId) {
        return customerRepository.findById(customerId).orElse(null);
    }

    @Override
    public Customer updateCustomer(Customer customer) {
        return customerRepository.saveAndFlush(customer);
    }

    @Override
    public Customer updateCustomerProfile( Customer customer, String profile) {
        customer.setProfile(profile);
        return customerRepository.saveAndFlush(customer);
    }

    @Override
    public BigDecimal getCurrentBalance(Customer customer){
        BigDecimal balance = BigDecimal.ZERO;
        List<Account> accountList = accountRepository.findByCustomerId(customer.getId());
        for(Account account: accountList){
            try {
                Consent consent = consentRepository.findById(account.getConsentId()).orElse(null);
                if(consent != null){
                    JSONObject postParam = new JSONObject();
                    postParam.put("linkReferenceNumber", account.getLinkRefNumber());
                    postParam.put("consentID", consent.getAaConsentId());
                    HttpEntity<String> requestBody = new HttpEntity<>(postParam.toString(), RestTemplateUtils.getAaHttpHeaders());
                    log.info("Request: {}", requestBody);
                    URI requestConsentURI = new URI(AAConstants.AA_BASE_URL + "getfibalance/");
                    ResponseEntity<String> response = restTemplate.postForEntity(requestConsentURI, requestBody, String.class);
                    String respStr = response.getBody();
                    log.info(respStr);
                    if(new JSONObject(respStr).getString("status").equals("success")){
                        JSONObject jsonObject = (JSONObject) new JSONObject(respStr).get("data");
                        BigDecimal accountBalance = jsonObject.getBigDecimal("balance");
                        balance = balance.add(accountBalance);
                    }
                }else{
                    log.info("No consent for account: {}", account);
                }
            }catch (Exception e){
                log.error("Exception while fetching balance for the account: {}", account);
            }
        }
        return balance;
    }
}
