package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.SpendingPattern;
import com.kaleidofin.aa.domain.Transactions;
import com.kaleidofin.aa.repository.CustomerRepository;
import com.kaleidofin.aa.repository.SpendingPatternRepository;
import com.kaleidofin.aa.service.SpendingPatternService;
import com.kaleidofin.aa.service.TransactionService;
import com.kaleidofin.aa.service.dto.SpendingPatternGroup;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SpendingPatternServiceImpl implements SpendingPatternService {

    private final Logger log = LoggerFactory.getLogger(SpendingPatternServiceImpl.class);

    @Autowired
    SpendingPatternRepository spendingPatternRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    TransactionService transactionService;

    private long months = 1;

    @Override
    public SpendingPatternGroup getSpendingPattern(Long customerId){
        Customer customer = customerRepository.findById(customerId).orElse(null);
        List<SpendingPattern> spendingPatternList = spendingPatternRepository.findByCustomerId(customerId);
        return calculateAmount(spendingPatternList, customer);
    }

    @Override
    public SpendingPatternGroup saveSpendingPatterns(Customer customer, List<SpendingPattern> spendingPatternList){
        for(SpendingPattern spendingPattern: spendingPatternList) {
            if (!spendingPattern.getCategory().equalsIgnoreCase("Total")) {
                spendingPattern.setCustomerId(customer.getId());
                spendingPattern = spendingPatternRepository.saveAndFlush(spendingPattern);
            }
        }
        return calculateAmount(spendingPatternList, customer);
    }

    @Override
    public SpendingPatternGroup calculateAmount(List<SpendingPattern> spendingPatternList, Customer customer){
        log.info("Spending Pattern List: {}", spendingPatternList);
        SpendingPatternGroup spendingPatternGroup = new SpendingPatternGroup();
        BigDecimal income = new BigDecimal(new JSONObject(customer.getProfile()).get("income").toString());
        SpendingPattern savingSpendingPattern = null;
        for(SpendingPattern spendingPattern: spendingPatternList){
            log.info("Spending Pattern: {}", spendingPattern);
            if (!spendingPattern.getCategory().equalsIgnoreCase("Total")) {
                log.info("Income: {}", income);
                BigDecimal amount = income.multiply(new BigDecimal(spendingPattern.getSpendPercentile())).divide(new BigDecimal(100));
                if(spendingPattern.getMinAmount() != null && amount.compareTo(spendingPattern.getMinAmount()) < 0)
                    amount =  spendingPattern.getMinAmount();
                else if(spendingPattern.getMaxAmount() != null && amount.compareTo(spendingPattern.getMaxAmount()) > 0)
                    amount =  spendingPattern.getMaxAmount();
                log.info("Amount: {}", amount);
                spendingPattern.setAmount(amount);
            }

            if(spendingPattern.getCategory().equalsIgnoreCase("Saving")){
                savingSpendingPattern = spendingPattern;
            }
        }
        spendingPatternGroup.setIncome(income);
        SpendingPattern totalSpendingPattern = calculateTotal(spendingPatternList);
        spendingPatternGroup.setTotalSpendingPattern(totalSpendingPattern);
        savingSpendingPattern = calculateSavingSpendingPattern(totalSpendingPattern, income, months);
        spendingPatternGroup.setSavingSpendingPattern(savingSpendingPattern);
        spendingPatternGroup.setSpendingPatternList(spendingPatternList);
        return spendingPatternGroup;
    }

    @Override
    public SpendingPattern calculateTotal(List<SpendingPattern> spendingPatternList){
        SpendingPattern totalSpendingPattern = new SpendingPattern();
        totalSpendingPattern.setCategory("Total");
        totalSpendingPattern.setSpendPercentile(0L);
        totalSpendingPattern.setAmount(BigDecimal.ZERO);
        for(SpendingPattern spendingPattern: spendingPatternList){
            if(!spendingPattern.getCategory().equalsIgnoreCase("Total")
                    && !spendingPattern.getCategory().equalsIgnoreCase("Saving")){
                totalSpendingPattern.setCustomerId(spendingPattern.getCustomerId());
                totalSpendingPattern.setSpendPercentile(totalSpendingPattern.getSpendPercentile() + spendingPattern.getSpendPercentile());
                totalSpendingPattern.setAmount(totalSpendingPattern.getAmount().add(spendingPattern.getAmount()));
            }
        }
        return totalSpendingPattern;
    }

    @Override
    public SpendingPatternGroup getTransactionSpendingPattern(Customer customer){
        SpendingPatternGroup spendingPatternGroup =  new SpendingPatternGroup();
        BigDecimal income = new BigDecimal(new JSONObject(customer.getProfile()).get("income").toString());
        List<Transactions> transactionsList = transactionService.getTransactions(customer);
        List<SpendingPattern> spendingPatternList = getSpendingPatternsForTransactions(customer, transactionsList);
        spendingPatternGroup.setIncome(income);
        SpendingPattern totalSpendingPattern = calculateTotal(spendingPatternList);
        spendingPatternGroup.setTotalSpendingPattern(totalSpendingPattern);
        SpendingPattern savingSpendingPattern = calculateSavingSpendingPattern(totalSpendingPattern, income, months);
        spendingPatternGroup.setSavingSpendingPattern(savingSpendingPattern);
        spendingPatternGroup.setSpendingPatternList(spendingPatternList);
        return spendingPatternGroup;
    }

    @Override
    public SpendingPatternGroup getTransactionSpendingPattern(Customer customer, Integer year, Integer month){
        SpendingPatternGroup spendingPatternGroup =  new SpendingPatternGroup();
        BigDecimal income = new BigDecimal(new JSONObject(customer.getProfile()).get("income").toString());
        List<Transactions> transactionsList = transactionService.getTransactions(customer, year, month);
        List<SpendingPattern> spendingPatternList = getSpendingPatternsForTransactions(customer, transactionsList);
        spendingPatternGroup.setIncome(income);
        SpendingPattern totalSpendingPattern = calculateTotal(spendingPatternList);
        spendingPatternGroup.setTotalSpendingPattern(totalSpendingPattern);
        SpendingPattern savingSpendingPattern = calculateSavingSpendingPattern(totalSpendingPattern, income, months>0?months:1);
        spendingPatternGroup.setSavingSpendingPattern(savingSpendingPattern);
        spendingPatternGroup.setSpendingPatternList(spendingPatternList);
        return spendingPatternGroup;
    }

    private List<SpendingPattern> getSpendingPatternsForTransactions(Customer customer, List<Transactions> transactionsList) {
        List<SpendingPattern> spendingPatternList = new ArrayList<>();
        Map<String, Map<String, List<Transactions>>> t = transactionsList.stream().collect(Collectors.groupingBy(Transactions::getType, Collectors.groupingBy(Transactions::getNarration)));
        if(t.containsKey("DEBIT")){
            Map<String, List<Transactions>> debt = t.get("DEBIT");
            List<Transactions> transactions = debt.values().stream().flatMap(List::stream).collect(Collectors.toList());
            BigDecimal totalCredit = transactions.stream().map(Transactions::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
            LocalDate minTransactionDate = transactions.stream().map(Transactions::getTransactionDate).min(LocalDate::compareTo).orElse(LocalDate.now());
            LocalDate maxTransactionDate = transactions.stream().map(Transactions::getTransactionDate).max(LocalDate::compareTo).orElse(LocalDate.now());
            months = ChronoUnit.MONTHS.between(YearMonth.from(minTransactionDate), YearMonth.from(maxTransactionDate));
            debt.forEach((k, v) -> {
                log.info("Key: {}, Total credit: {}", k, totalCredit);
                SpendingPattern spendingPattern = new SpendingPattern();
                spendingPattern.setCustomerId(customer.getId());
                spendingPattern.setCategory(k);
                spendingPattern.setAmount(v.stream().map(Transactions::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add));
                log.info("SP: {}", spendingPattern);
                BigDecimal multiply = spendingPattern.getAmount().multiply(new BigDecimal(100));
                BigDecimal percentile = multiply.divide(totalCredit, RoundingMode.CEILING);
                log.info("mul: {} , percent: {}", multiply, percentile);
                spendingPattern.setSpendPercentile(percentile.longValue());
                log.info("Spending Pattern: {}", spendingPattern);
                spendingPatternList.add(spendingPattern);
            });
        }
        return spendingPatternList;
    }

    private SpendingPattern calculateSavingSpendingPattern(SpendingPattern total, BigDecimal income, long months){
        SpendingPattern savingSpendingPattern =  new SpendingPattern();
        savingSpendingPattern.setCustomerId(total.getCustomerId());
        savingSpendingPattern.setCategory("Saving");
//        BigDecimal totalIncome = income.multiply(new BigDecimal(months));
        BigDecimal totalIncome = income;
        savingSpendingPattern.setAmount(totalIncome.subtract(total.getAmount()));
        savingSpendingPattern.setSpendPercentile(savingSpendingPattern.getAmount().multiply(new BigDecimal(100)).divide(totalIncome, RoundingMode.CEILING).longValue());
        return savingSpendingPattern;
    }

}
