package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.Goal;
import com.kaleidofin.aa.repository.GoalsRepository;
import com.kaleidofin.aa.service.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class GoalServiceImpl implements GoalService {

    @Autowired
    GoalsRepository goalsRepository;

    @Override
    public Goal createGoal(Customer customer, String goalName, BigDecimal goalAmount, Long tenure){
        Goal goal = new Goal();
        goal.setCustomerId(customer.getId());
        goal.setGoalName(goalName);
        goal.setGoalAmount(goalAmount);
        goal.setTenture(tenure);
        return createGoal(goal);
    }

    @Override
    public Goal createGoal(Goal goal){
        return goalsRepository.saveAndFlush(goal);
    }

    @Override
    public List<Goal> getGoals(Customer customer){
        return goalsRepository.findByCustomerId(customer.getId());
    }
}
