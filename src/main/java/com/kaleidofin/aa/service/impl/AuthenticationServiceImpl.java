package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.User;
import com.kaleidofin.aa.repository.CustomerRepository;
import com.kaleidofin.aa.repository.UserRepository;
import com.kaleidofin.aa.service.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private final Logger log = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer authenticateUser(String mobile, String UUID) {
        User user = userRepository.findByLogin(mobile);
        log.info("user: {}", user);
        if(user == null){
            Customer customer = new Customer();
            customer.setMobile(mobile);
            customer = customerRepository.saveAndFlush(customer);

            log.info("Customer: {}", customer);

            user = new User();
            user.setLogin(mobile);
            user.setCustomerId(customer.getId());
        }
        user.setSessionId(UUID);
        userRepository.saveAndFlush(user);
        return customerRepository.findById(user.getCustomerId()).orElse(null);
    }

    @Override
    public Customer validateCustomer(String sessionId){
        User user = userRepository.findBySessionId(sessionId);
        return  customerRepository.findById(user.getCustomerId()).orElse(null);
    }


}
