package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.constants.AAConstants;
import com.kaleidofin.aa.domain.Account;
import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.Transactions;
import com.kaleidofin.aa.repository.AccountRepository;
import com.kaleidofin.aa.repository.ConsentRepository;
import com.kaleidofin.aa.repository.CustomerRepository;
import com.kaleidofin.aa.repository.TransactionRepository;
import com.kaleidofin.aa.service.TransactionService;
import com.kaleidofin.aa.utils.RestTemplateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ConsentRepository consentRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public void fetchTransactions(Consent consent) {
        List<Account> accountList = accountRepository.findByConsentId(consent.getId());

        for(Account account : accountList){
            try {
                fetchTransactions(consent, account);
            } catch (Exception e) {
                log.error("Error fetching transactions for Account: {}", account.getId(), e);
            }
        }
    }

    @Override
    public Consent fetchTransactions(Consent consent, Account account) throws URISyntaxException {
        try{
            JSONObject postParam = new JSONObject();
            postParam.put("linkReferenceNumber", account.getLinkRefNumber());
            postParam.put("consentID", consent.getAaConsentId());
            HttpEntity<String> requestBody = new HttpEntity<>(postParam.toString(), RestTemplateUtils.getAaHttpHeaders());
            log.info("Request: {}", requestBody);
            URI requestConsentURI = new URI(AAConstants.AA_BASE_URL + "getfidata/");
            ResponseEntity<String> response = restTemplate.postForEntity(requestConsentURI, requestBody, String.class);
            String respStr = response.getBody();
            log.info(respStr);
            if(new JSONObject(respStr).getString("status").equals("success")){
                JSONObject jsonObject = (JSONObject) new JSONObject(respStr).get("data");
                JSONArray transactionData = jsonObject.getJSONArray("fiData");
                for(int i=0; i<transactionData.length(); i++){
                    try{
                        JSONObject transactionObj = transactionData.getJSONObject(i);
                        Transactions transaction = transactionRepository.findByTransactionId(transactionObj.getString("txnId"));
                        if(transaction == null){
                            transaction = new Transactions();
                            transaction.setCustomerId(consent.getCustomerId());
                            transaction.setAccountId(account.getId());
                            LocalDate transactionDate = getLocalDate(transactionObj.getString("valueDate"));
                            transaction.setTransactionDate(transactionDate);
                            transaction.setTransactionId(transactionObj.getString("txnId"));
                            transaction.setAmount(new BigDecimal(transactionObj.getString("amount")));
                            transaction.setBalance(new BigDecimal(transactionObj.getString("balance")));
                            transaction.setMode(transactionObj.getString("mode"));
                            transaction.setType(transactionObj.getString("type"));
                            transaction.setNarration(transactionObj.getString("narration"));
                            transactionRepository.saveAndFlush(transaction);
                        }else{
                            log.info("Transaction, {} already fetched", transaction.getId());
                        }
                    }catch (Exception e){
                        log.error("Error Adding Transaction: {}", transactionData.getJSONObject(i), e);
                    }
                }
                consent.setDataFetched(true);
                consent = consentRepository.saveAndFlush(consent);
            }
        }catch (Exception e){
            log.error("Error fetching Transactions for account - {} and consent - {}", account, consent, e);
        }
        return consent;
    }

    private LocalDate getLocalDate(String date) {
        try{
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            return LocalDate.parse(date, inputFormatter);
        }catch (Exception e){
            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
            return LocalDate.parse(date, inputFormatter);
        }
    }

    @Override
    public List<Transactions> getTransactions(Customer customer){
        return transactionRepository.findByCustomerId(customer.getId());
    }

    @Override
    public List<Transactions> getTransactions(Customer customer, Integer year, Integer month){
        return transactionRepository.findByCustomerIdForAMonth(customer.getId(), year, month);
    }




}
