package com.kaleidofin.aa.service.impl;

import com.kaleidofin.aa.constants.AAConstants;
import com.kaleidofin.aa.domain.Account;
import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.repository.AccountRepository;
import com.kaleidofin.aa.repository.ConsentRepository;
import com.kaleidofin.aa.repository.CustomerRepository;
import com.kaleidofin.aa.service.ConsentService;
import com.kaleidofin.aa.service.TransactionService;
import com.kaleidofin.aa.utils.RestTemplateUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class ConsentServiceImpl implements ConsentService {

    private final Logger log = LoggerFactory.getLogger(ConsentServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ConsentRepository consentRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    TransactionService transactionService;

    @Override
    public Consent requestConsent(Customer customer, String productId, String accountId) throws URISyntaxException {
        productId = productId == null ? AAConstants.AA_PRODUCT_ID : productId;
        org.json.JSONObject postParam = new JSONObject();
        postParam.put("partyIdentifierType", AAConstants.AA_PARTY_IDENTIFIER_TYPE);
        postParam.put("partyIdentifierValue", customer.getMobile());
        postParam.put("productID", productId);
        postParam.put("accountID", accountId);
        postParam.put("vua", customer.getAaVui());
        HttpEntity<String> requestBody = new HttpEntity<>(postParam.toString(), RestTemplateUtils.getAaHttpHeaders());
        log.info("Request: {}", requestBody);
        URI requestConsentURI = new URI(AAConstants.AA_BASE_URL_V2 + "requestconsent/");
        ResponseEntity<String> response = restTemplate.postForEntity(requestConsentURI, requestBody, String.class);
        String respStr = response.getBody();
        log.info(respStr);
        JSONObject jsonObject = (JSONObject) new JSONObject(respStr).get("data");
        return createConsent(customer, accountId, productId, jsonObject.get("consent_handle").toString());
    }

    @Override
    public Consent createConsent(Customer customer, String accountId, String productId, String consentHandle){
        productId = productId == null ? AAConstants.AA_PRODUCT_ID : productId;
        Consent consent = new Consent();
        consent.setConsentHandle(consentHandle);
        consent.setAccountId(accountId);
        consent.setCustomerId(customer.getId());
        consent.setProductId(productId);
        consent.setStatus("PENDING");
        consent.setDataFetched(false);
        return consentRepository.saveAndFlush(consent);
    }

    @Override
    public Consent checkConsentStatus(Consent consent) throws URISyntaxException {
        Customer customer = customerRepository.findById(consent.getCustomerId()).orElse(null);
        JSONObject postParam = new JSONObject();
        postParam.put("partyIdentifierType", AAConstants.AA_PARTY_IDENTIFIER_TYPE);
        postParam.put("partyIdentifierValue", customer.getMobile());
        postParam.put("productID", consent.getProductId());
        postParam.put("accountID", consent.getAccountId());
        HttpEntity<String> requestBody = new HttpEntity<>(postParam.toString(), RestTemplateUtils.getAaHttpHeaders());
        log.info("Request: {}", requestBody);
        URI requestConsentURI = new URI(AAConstants.AA_BASE_URL_V2 + "getconsentslist/");
        ResponseEntity<String> response = restTemplate.postForEntity(requestConsentURI, requestBody, String.class);
        String respStr = response.getBody();
        log.info(respStr);
        JSONArray consentData = (JSONArray) new JSONObject(respStr).get("data");
        for(int i=0; i<consentData.length(); i++){
            try{
                JSONObject consentObj = consentData.getJSONObject(i);
                consent = consentRepository.findByConsentHandle(consentObj.getString("consentHandle")).orElse(consent);
                if(consent.getStatus().equals("PENDING")){
                    consent.setAaConsentId(consentObj.getString("consentID"));
                    consent.setStatus(consentObj.getString("status"));
                    customer.setAaVui(consentObj.getString("vua"));
                    consent.setConsentHandle(consentObj.getString("consentHandle"));
                    customer = customerRepository.saveAndFlush(customer);
                    consent = consentRepository.saveAndFlush(consent);
                    JSONArray accountData = consentObj.getJSONArray("accounts");
                    for(int j=0; i<accountData.length(); j++){
                        try{
                            JSONObject accountObj = accountData.getJSONObject(j);
                            Account account = accountRepository.findByCustomerIdAndFipIdAndLinkRefNumber(customer.getId(),
                                    accountObj.getString("fipId"), accountObj.getString("linkReferenceNumber"));
                            if(account == null){
                                account = new Account();
                                account.setCustomerId(customer.getId());
                                account.setConsentId(consent.getId());
                                account.setFipId(accountObj.getString("fipId"));
                                account.setFipName(accountObj.getString("fipName"));
                                account.setAccountType(accountObj.getString("accountType"));
                                account.setFiType(accountObj.getString("fiType"));
                                account.setLinkRefNumber(accountObj.getString("linkReferenceNumber"));
                                account.setMaskedAccountNo(accountObj.getString("maskedAccountNumber"));
                                account = accountRepository.saveAndFlush(account);
                                consent = transactionService.fetchTransactions(consent, account);
                            }else{
                                log.info("Account, {} already fetched", account.getId());
                            }
                        }catch (Exception e){
                                log.error("Error Adding Account: {}", accountData.getJSONObject(j), e);
                        }
                    }
                }
            }catch (Exception e){
                log.error("Error processing consent: {}", consentData.getJSONObject(i), e);
            }
        }
        return consent;
    }

    @Override
    public List<Consent> getConsent(Customer customer, String productId, String accountId) {
        return consentRepository.findByCustomerIdAndAccountIdAndProductId(customer.getId(), accountId, productId);
    }

}
