package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.domain.Customer;

import java.net.URISyntaxException;
import java.util.List;

public interface ConsentService {
    Consent requestConsent(Customer customer, String productId, String accountId) throws URISyntaxException;

    Consent createConsent(Customer customer, String accountId, String productId, String consentHandle);

    Consent checkConsentStatus(Consent consent) throws URISyntaxException;

    List<Consent> getConsent(Customer customer, String productId, String accountId);
}
