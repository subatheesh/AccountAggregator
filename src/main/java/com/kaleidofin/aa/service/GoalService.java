package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Customer;
import com.kaleidofin.aa.domain.Goal;

import java.math.BigDecimal;
import java.util.List;

public interface GoalService {
    Goal createGoal(Customer customer, String goalName, BigDecimal goalAmount, Long tenure);

    Goal createGoal(Goal goal);

    List<Goal> getGoals(Customer customer);
}
