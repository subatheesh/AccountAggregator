package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Customer;

import java.math.BigDecimal;

public interface CustomerService {

    public Customer getCustomer(Long id);

    public Customer updateCustomer(Customer customer);

    Customer updateCustomerProfile(Customer customer, String profile);

    BigDecimal getCurrentBalance(Customer customer);
}
