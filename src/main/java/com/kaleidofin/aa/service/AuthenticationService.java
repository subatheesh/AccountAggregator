package com.kaleidofin.aa.service;

import com.kaleidofin.aa.domain.Customer;

public interface AuthenticationService {
    Customer authenticateUser(String mobile, String UUID);

    Customer validateCustomer(String sessionId);
}
