package com.kaleidofin.aa.schedule;

import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.repository.ConsentRepository;
import com.kaleidofin.aa.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionFetchSchedular {

    private final Logger log = LoggerFactory.getLogger(TransactionFetchSchedular.class);

    @Autowired
    ConsentRepository consentRepository;

    @Autowired
    TransactionService transactionService;

    @Scheduled(cron = "0 1/2 * * * ?", zone = "Asia/Calcutta")
    public void fetchTransactions(){
        List<Consent> consentList = consentRepository.findByStatusAndIsDataFetched("ACTIVE", false);
        for(Consent consent: consentList) {
            try {
                transactionService.fetchTransactions(consent);
            }catch (Exception e){
                log.error("Exception in fetching transaction for consent: {}", consent.getId());
            }
        }
    }
}
