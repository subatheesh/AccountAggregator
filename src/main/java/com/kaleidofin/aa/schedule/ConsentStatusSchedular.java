package com.kaleidofin.aa.schedule;

import com.kaleidofin.aa.domain.Consent;
import com.kaleidofin.aa.repository.ConsentRepository;
import com.kaleidofin.aa.service.ConsentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsentStatusSchedular {

    private final Logger log = LoggerFactory.getLogger(ConsentStatusSchedular.class);

    @Autowired
    ConsentRepository consentRepository;

    @Autowired
    ConsentService consentService;

    @Scheduled(cron = "0 0/2 * * * ?", zone = "Asia/Calcutta")
    public void checkConsentStatus(){

        List<Consent> consents = consentRepository.findByStatus("PENDING");
        for(Consent consent : consents){
            try {
                consentService.checkConsentStatus(consent);
            } catch (Exception e) {
                log.error("Exception in Consent Status Check: {}", consent.getId(), e);
            }
        }

    }

}
