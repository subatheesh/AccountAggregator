--liquibase formatted sql

--changeset Subatheesh:20200720161201

create table customer(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `name` varchar(100) NULL DEFAULT NULL,
    `mobile` bigint(20) NULL DEFAULT NULL,
    `email` varchar(100) NULL DEFAULT NULL,
    `aa_vui` varchar(100) NULL DEFAULT NULL,
    `profile` text NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

create table consent(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NULL DEFAULT NULL,
    `aa_consent_id` varchar(100) NULL DEFAULT NULL,
    `consent_handle` varchar(100) NULL DEFAULT NULL,
    `is_data_fetched` bit(1) NOT NULL DEFAULT false,
    `status` varchar(100) NULL DEFAULT NULL,
    `product_id` varchar(100) NULL DEFAULT NULL,
    `account_id` varchar(100) NULL DEFAULT NULL,
    `type` varchar(100) NULL DEFAULT NULL,
    `frequency` varchar(100) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

create table account(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NOT NULL,
    `aa_consent_id` bigint(20) NOT NULL,
    `balance` decimal(20) NULL DEFAULT NULL,
    `fip_name` varchar(200) NULL DEFAULT NULL,
    `fip_id` varchar(100) NULL DEFAULT NULL,
    `account_type` varchar(100) NULL DEFAULT NULL,
    `link_ref_number` varchar(100) NULL DEFAULT NULL,
    `masked_account_no` varchar(100) NULL DEFAULT NULL,
    `fi_type` varchar(100) NULL DEFAULT NULL,
    `institute_name` varchar(100) NULL DEFAULT NULL,
    `link_ref_number` varchar(100) NULL DEFAULT NULL,
    `account_holder_type` varchar(100) NULL DEFAULT NULL,
    `transaction_start_date` date NULL DEFAULT NULL,
    `transaction_end_date` date NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;



create table account_holder_profile(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NOT NULL,
    `account_id` bigint(20) NOT NULL,
    `name` varchar(200) NULL DEFAULT NULL,
    `date_of_birth` date NULL DEFAULT NULL,
    `mobile` bigint(20) NULL DEFAULT NULL,
    `nominee` varchar(100) NULL DEFAULT NULL,
    `landline` bigint(20) NULL DEFAULT NULL,
    `address` varchar(100) NULL DEFAULT NULL,
    `email` varchar(50) NULL DEFAULT NULL,
    `pan` varchar(50) NULL DEFAULT NULL,
    `ckyc_compilance` varchar(100) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;


create table transactions(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NOT NULL,
    `account_id` bigint(20) NOT NULL,
    `transaction_id` varchar(200) NULL DEFAULT NULL,
    `mode` varchar(100) NULL DEFAULT NULL,
    `amount` decimal(10) NULL DEFAULT NULL,
    `balance` decimal(10) NULL DEFAULT NULL,
    `type` varchar(50) NULL DEFAULT NULL,
    `narration` text NULL DEFAULT NULL,
    `email` varchar(50) NULL DEFAULT NULL,
    `reference_no` varchar(50) NULL DEFAULT NULL,
    `transaction_date` date NULL DEFAULT NULL,
    `transaction_fetch_timestamp` timestamp NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;



create table user(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `login` varchar(100) NOT NULL,
    `customer_id` bigint(20) NOT NULL,
    `session_id` varchar(100) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;



create table account_summary(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NOT NULL,
    `account_id` bigint(20) NOT NULL,
    `currency` varchar(10) NULL DEFAULT NULL,
    `exchange_rate` varchar(20) NULL DEFAULT NULL,
    `balance_date_time` timestamp NULL DEFAULT NULL,
    `type` varchar(100) NULL DEFAULT NULL,
    `branch` varchar(100) NULL DEFAULT NULL,
    `facility` varchar(100) NULL DEFAULT NULL,
    `ifsc_code` varchar(100) NULL DEFAULT NULL,
    `micro_code` varchar(100) NULL DEFAULT NULL,
    `opening_date` date NULL DEFAULT NULL,
    `current_od_limit` varchar(100) NULL DEFAULT NULL,
    `drawing_limit` date NULL DEFAULT NULL,
    `maturity_date` date NULL DEFAULT NULL,
    `maturity_amount` decimal(10,2) NULL DEFAULT NULL,
    `description` varchar(1000) NULL DEFAULT NULL,
    `interest_payout` varchar(100) NULL DEFAULT NULL,
    `interest_rate` decimal(10,2) NULL DEFAULT NULL,
    `principle_amount` varchar(10) NULL DEFAULT NULL,
    `tenure_days` varchar(20) NULL DEFAULT NULL,
    `tenure_months` timestamp NULL DEFAULT NULL,
    `tenure_years` varchar(100) NULL DEFAULT NULL,
    `recurring_amount` varchar(100) NULL DEFAULT NULL,
    `recurringDepositDay` varchar(100) NULL DEFAULT NULL,
    `interestComputation` varchar(100) NULL DEFAULT NULL,
    `compoundingFrequency` varchar(100) NULL DEFAULT NULL,
    `interestPeriodicPayoutAmount` varchar(100) NULL DEFAULT NULL,
    `interestOnMaturity` varchar(100) NULL DEFAULT NULL,
    `currentValue` varchar(100) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

create table spending_pattern(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NULL DEFAULT NULL,
    `category` varchar(200) NULL DEFAULT NULL,
    `min_amount` decimal(10,2) NULL DEFAULT NULL,
    `max_amount` decimal(10,2) NULL DEFAULT NULL,
    `spend_percentile` decimal(10,2) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Food', 800, 50000, 15);
insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Travel', 0, 10000, 10);
insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Health', 500, 50000, 18);
insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Entertainment', 0, 12000, 20);
insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Saving', 200, 150000, 20);
insert into spending_pattern(customer_id, category, min_amount, max_amount, spend_percentile) values (null, 'Bills', 800, 100000, 17);

create table goals(
    `id` bigint(20) NOT NULL auto_increment,
    `version` int(11) NOT NULL DEFAULT '0',
    `customer_id` bigint(20) NULL DEFAULT NULL,
    `goal_name` varchar(200) NULL DEFAULT NULL,
    `goal_amount` decimal(10,2) NULL DEFAULT NULL,
    `tenture` int(10) NULL DEFAULT NULL,
    `created_by` varchar(50) NOT NULL DEFAULT 'system',
    `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_modified_by` varchar(50) DEFAULT NULL,
    `last_modified_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;